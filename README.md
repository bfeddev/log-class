# GWLogs

A utility class for logging messages, events or errors either to a file or the system event logs with a configurable level of verbosity.    


Usage Examples:  
$logOptions = array (  
  'ext'                       => '.log',              // allows for changing the log file extention                     
  'filename'                  => 'testing',           // allows for changing the file name  
  'includeFNDate'             => true,                // allows for including the date in the file name  
  'messageFormat'             => true,                // allows for formatting the message or leaving it generic  
  'messageDateFormat'         => 'Y-m-d H:i:s',       // allows for adjusting the date format used in the log entry  
  'includeBackTrace'          => false,               // allows for including PHP's debug_backtrace()  
);    

// LINUX  
$log = new GWLogs('/home/bretfedd/public_html/gwlogs/', 'DEBUG');  
$log->Log('INFO', 'Testing, 1, 2, 3');  
$log->Log('NOTICE', 'Another Test');  

// WINDOWS  
$another = new GWLogs('C:\inetpub\wwwroot\logtest\logs', 'DEBUG', $logOptions);  
$another->Log('INFO', 'File Name: ' . $another->GetFilename());  