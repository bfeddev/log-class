<?php 

/**
 *   A utility class for logging messages, events or errors either to a file or the system event logs with a 
 *   configurable level of verbosity.
 *
 *   usage examples:
 *   $logOptions = array (
 *     'ext'                       => '.log',              // allows for changing the log file extention                   
 *     'filename'                  => 'testing',           // allows for changing the file name
 *     'includeFNDate'             => true,                // allows for including the date in the file name
 *     'messageFormat'             => true,                // allows for formatting the message or leaving it generic
 *     'messageDateFormat'         => 'Y-m-d H:i:s',       // allows for adjusting the date format used in the log entry
 *     'includeBackTrace'          => false,               // allows for including PHP's debug_backtrace()
 *   );
 *   
 *   $log = new GWLogs('/app/storage/logs/', 'DEBUG');
 *   $log->Log('INFO', 'Testing, 1, 2, 3');
 *   $log->Log('NOTICE', 'Another Test');
 *   
 *   $another = new GWLogs('C:\inetpub\wwwroot\logtest\logs', 'DEBUG', $logOptions);
 *   $another->Log('INFO', 'File Name: ' . $another->GetFilename());
 *
 *   @author  Bret Feddern <bfeddern at hotmail dot com>
 *   @version 1.0.0
 */
    
    class GWLogs {

        /**
         *   path to the log file
         *   @var string
         */
        private $logFilePath;

        /**
         *   name of the log file
         *   @var string
         */
        private $logFileName;

        /**
         *   holds the file handle for the log file
         *   @var resource
         */
        private $logFileHandle;

        /**
         *   minimum logging threshold
         *   @var string
         */
        protected $logLevelThreshold = 'DEBUG';
        
        /**
         *   options for storing the log data
         *   @var array
         */
        protected $logOptions = array (
            'ext'                       => '.log',              // allows for changing the log file extention                   
            'filename'                  => 'gwlogs',            // allows for changing the file name
            'includeFNDate'             => false,               // allows for including the date in the file name
            'messageFormat'             => true,                // allows for formatting the message or leaving it generic
            'messageDateFormat'         => 'Y-m-d H:i:s',       // allows for adjusting the date format used in the log entry
            'includeBackTrace'          => true,                // allows for including PHP's debug_backtrace()
        );

        /**
         *   possible severity levels for logs
         *   @var array
         */
        protected $logLevels = array(
            'EMERGENCY'         => 0,
            'ALERT'             => 1,
            'CRITICAL'          => 2,
            'ERROR'             => 3,
            'WARNING'           => 4,
            'NOTICE'            => 5,
            'INFO'              => 6,
            'DEBUG'             => 7
        );

        /**
         *   class constructor
         *
         *   @param string $directory = path to the directory where the log file is or will be stored
         *   @param string $level = the log level threshold
         *   @param array  $options = configurable message writing options
         */
        public function __construct($directory, $level = null, $options = array()) 
        {
            $this->logFilePath = $directory;

            // if a custom threshold is passed in, update the variable
            if($level != null) {
                $this->logLevelThreshold = $level;
            }

            // if custom options are passed in, update the array
            if(!empty($options)) {
                $this->logOptions = array_merge($this->logOptions, $options);
            }

            // if the directory for the log file does not exist, create it with proper permissions
            $directory = rtrim($directory, DIRECTORY_SEPARATOR);

            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);               
            }

            // set the file name for the log file, based on optional parameters
            $this->logFileName = $this->logOptions['includeFNDate'] ? $this->logOptions['filename'] . '-' . date('Y-m-d') . $this->logOptions['ext'] : $this->logOptions['filename'] . $this->logOptions['ext'];

            // if the log file exists, it better be writable.  if not, no soup for you.
            if(file_exists($this->logFileName) && !is_writable($this->logFileName)) {
                throw new RuntimeException('Could not write to log file. Check that the correct file permissions have been set.');
            }
        }

        /**
         *   get the threshold of the current instance for the logging
         *
         *   @return string
         */
        public function GetThreshold() 
        {
            return $this->logLevelThreshold . ' (' . $this->logLevels[$this->logLevelThreshold] . ')';
        }

        /**
         *   get the full path and file name for the log file
         *
         *   @return string
         */
        public function GetFilename() 
        {
            return $this->logFilePath . DIRECTORY_SEPARATOR . $this->logFileName;
        }

        /**
         *   processes command for writing a message based on parameters passed in
         * 
         *   @param string $logLevel = the level/severity of the log being passed in.
         *   @param string $logMessage = custom message passed in to be used in output.
         *   @param string $customWriteMode = if something other than 'a' is needed
         *   @return void
         */
        public function Log($logLevel, $logMessage, $customWriteMode = null)
        {
            // threshold check for environment adapation (only log certain levels in prod, etc.)
            if ($this->logLevels[$this->logLevelThreshold] < $this->logLevels[$logLevel]) {
                return;
            }

            $message = $this->FormatLogString($logLevel, $logMessage) . PHP_EOL;
            
            if($customWriteMode !== null) {
                $this->Write($message, "$customWriteMode");
            } else {
                $this->Write($message);
            }
        }

        /**
         *   formats message based on parameters passed in and log options 
         * 
         *   @param string $logLevel = the level/severity of the log being passed in.
         *   @param string $logMessage = custom message passed in to be used in output.
         *   @return void
         */
        private function FormatLogString($logLevel, $logMessage) 
        {
            $logDateTime = date($this->logOptions['messageDateFormat']);

            // check if message formatting is on
            if ($this->logOptions['messageFormat']) {

                // basic formatted message
                $returnMessage = "[$logDateTime] " . strtoupper($logLevel) . ":   $logMessage";

                // if the backtrace is turned on, include that into the log message
                if ($this->logOptions['includeBackTrace']) {
                    $returnMessage = $returnMessage . ' Backtrace:   ' . json_encode(debug_backtrace());
                }

            } else {
                $returnMessage = $logDateTime . ' ' . $logLevel . ': ' .  $logMessage;
            }

            return $returnMessage;
        }

        /**
         *   writes a line to the file 
         *
         *   @param string $writeMode = 'a' by default (see mode: https://www.php.net/manual/en/function.fopen.php)
         *   @param string $message Line to write to the log
         *   @return void
         */
        private function Write($message, $writeMode = 'a')
        {
            // if file exists, write to it.  if not, create it then write to it.
            if(file_exists($this->GetFilename())) {
                $this->logFileHandle = fopen($this->GetFilename(), "$writeMode");
            } else {
                $this->logFileHandle = fopen($this->GetFilename(), "w");
            }

            if (!$this->logFileHandle) {
                throw new RuntimeException('Log file could not be opened. Please double-check the file permissions.');
            }

            if (fwrite($this->logFileHandle, $message) === false) {
                throw new RuntimeException('Log file could not be written to. Please double-check the file permissions.');
            } else {
                // may want to add logic here
            }

            fclose($this->logFileHandle);
        }
	}
?>